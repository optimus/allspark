<?php
$input = (object) array();

$input->origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : $_SERVER['SERVER_NAME'];
$input->url = parse_url($_SERVER['REQUEST_URI']);
$input->path = explode('/',substr($input->url['path'],1));
$input->server = substr($_SERVER['SERVER_NAME'],4);

if (is_dir('api_'.$input->path[0]))
	include_once 'api_'.$input->path[0] . '/index.php';
else
{
	http_response_code(400);
	die(json_encode(array("code" => 400, "message" => "Module API inconnu")));
}
?>