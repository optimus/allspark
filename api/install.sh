#!/bin/bash
source /etc/allspark/functions.sh
if [ -z $DOMAIN ]; then require DOMAIN string "Veuillez indiquer votre nom de domaine :"; source /root/.allspark; fi
if [ -z $ALLSPARK_API_MARIADB_USER ]; then require ALLSPARK_API_MARIADB_USER string "Veuillez renseigner le nom de l'utilisateur MARIADB pour l'api ALLSPARK :"; source /root/.allspark; fi
if [ -z $ALLSPARK_API_MARIADB_PASSWORD ] || [ $ALLSPARK_API_MARIADB_PASSWORD = "auto" ]; then require ALLSPARK_API_MARIADB_PASSWORD password "Veuillez renseigner le mot de passe de l'utilisateur MARIADB pour l'api ALLSPARK :"; source /root/.allspark; fi
if [ -z $AES_KEY ] || [ $AES_KEY = "auto" ]; then require AES_KEY aeskey "Veuillez renseigner une clé de chiffrement AES de 16 caractères [A-Za-z0-9]"; source /root/.allspark; fi
if [ -z $API_SHA_KEY ] || [ $API_SHA_KEY = "auto" ]; then require API_SHA_KEY aeskey "Veuillez renseigner une clé de chiffrement SHA de 16 caractères [A-Za-z0-9]"; source /root/.allspark; fi
if [ -z $MODULE_API ]; then require MODULE_API yesno "Voulez-vous installer l'espace d'hébergement api.$DOMAIN ?"; source /root/.allspark; fi
if [ -z $ALLSPARK_ADMIN_USER ]; then require ALLSPARK_ADMIN_USER string "Veuillez renseigner le nom du premier administrateur ALLSPARK :"; source /root/.allspark; fi
if [ -z $ALLSPARK_ADMIN_FIRSTNAME ]; then require ALLSPARK_ADMIN_FIRSTNAME string "Veuillez renseigner le prénom du premier administrateur ALLSPARK :"; source /root/.allspark; fi
if [ -z $ALLSPARK_ADMIN_LASTNAME ]; then require ALLSPARK_ADMIN_LASTNAME string "Veuillez renseigner le nom de famille du premier administrateur ALLSPARK :"; source /root/.allspark; fi
if [[ $ALLSPARK_ADMIN_USER =~ ^[YyOo]$ ]] && { [ -z $ALLSPARK_ADMIN_PASSWORD ] || [ $ALLSPARK_ADMIN_PASSWORD = "auto" ]; }; then require ALLSPARK_ADMIN_PASSWORD password "Veuillez renseigner le mot de passe de connexion du premier administrateur ALLSPARK :"; source /root/.allspark; fi

source /root/.allspark

if [ $MODULE_API = "Y" ]
then
  echo
  echo_green "==== INSTALLATION DE L'ESPACE D'HERGEMENT API ===="

  echo_magenta "Création de l'espace d'hébergement api.$DOMAIN..."
  if [ ! -d "/srv/api" ]; then verbose mkdir /srv/api; fi
  if [ ! -f "/etc/apache2/sites-enabled/api.conf" ]; then sed -e 's/%DOMAIN%/'$DOMAIN'/g' /etc/allspark/api/vhost > /etc/apache2/sites-enabled/api.conf; fi
  mkdir -p /srv/api/tmp
  chown -R www-data:www-data /srv/api

  cd /srv/api
  cp -R /etc/allspark/api/install/. /srv/api/
  envsubst '${ALLSPARK_API_MARIADB_USER} ${ALLSPARK_API_MARIADB_PASSWORD} ${DOMAIN} ${AES_KEY} ${API_SHA_KEY}' < /etc/allspark/api/install/config.php > /srv/api/config.php

  echo_magenta "Installation de l'API ALLSPARK"
  verbose rm -f -R /srv/api/api_allspark
  verbose mkdir -p /srv/api/api_allspark
  git clone --quiet https://git.cybertron.fr/allspark/api /srv/api/api_allspark
  chown -R www-data:www-data /srv/api/api_allspark

  echo_magenta "Installation de la base de données 'server'"
  for file in /srv/api/api_allspark/sql/*.sql
  do
    file="${file:26:-4}"
    if [[ $file > $ALLSPARK_API_DB_VERSION ]]
    then
      echo_magenta "--> $file.sql exécuté"
      mariadb < /srv/api/api_allspark/sql/$file.sql
      update_conf ALLSPARK_API_DB_VERSION $file
    else
      echo_magenta "--> $file.sql ignoré"
    fi
  done
  verbose mariadb -u root -e "REPLACE INTO server.services SET name='allspark', displayname='Allspark', status=1, install='/etc/allspark/api/install.sh', uninstall=null, description='Framework de gestion des modules et des utilisateurs'"
  
  echo_magenta "Création du premier administrateur allspark"
  verbose mariadb -u root -e "INSERT IGNORE INTO server.users VALUES (1, '$DOMAIN', 1, 1, '$ALLSPARK_ADMIN_USER@$DOMAIN', '$ALLSPARK_ADMIN_FIRSTNAME', '$ALLSPARK_ADMIN_LASTNAME', AES_ENCRYPT('$ALLSPARK_ADMIN_PASSWORD','$AES_KEY'));"
  verbose mariadb -u root -e "FLUSH PRIVILEGES;"

  echo_magenta "Attributions des droits sudo à l'api ALLSPARK pour certaines commandes"
  cp -f /etc/allspark/api/www-data /etc/sudoers.d/www-data
  chmod 440 /etc/sudoers.d/www-data
  
  echo_magenta "Creation de l'utilisateur MARIADB"
  verbose mariadb -u root -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER, GRANT OPTION ON *.* TO '$ALLSPARK_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$ALLSPARK_API_MARIADB_PASSWORD';"
  verbose mariadb -u root -e "GRANT SELECT, INSERT, UPDATE, DELETE ON server.* TO '$ALLSPARK_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$ALLSPARK_API_MARIADB_PASSWORD';"
  verbose mariadb -u root -e "FLUSH PRIVILEGES;"

  echo_magenta "Ajout du domaine dans les origines autorisées"
  verbose mariadb -u root -e "REPLACE INTO server.allowed_origins SET id=1, origin='*.$DOMAIN'"
  verbose mariadb -u root -e "REPLACE INTO server.allowed_origins SET id=2, origin='*.optimus-avocats.fr'"

  echo_magenta "Redémarrage des services"
  verbose systemctl restart apache2
fi
