<?php

$labels = array();

// mail

$labels['folder_contact'] = 'Dossier Contact';

$labels['folder_create'] = 'Créer un dossier';
$labels['folder_delete'] = 'Supprimer un dossier';
$labels['folder_rename'] = 'Renommer un dossier';
$labels['folder_locate'] = 'Localiser un dossier';

$labels['folder_purge'] = 'Purger le dossier';

$labels['folder_read_this'] = 'Marquer le dossier comme lu';
$labels['folder_read_tree'] = 'Marquer dossier + sous dossiers comme lus';

$labels['show_all'] = 'Afficher tout';
$labels['show_active'] = 'Afficher actifs';
$labels['show_favorite'] = 'Afficher favoris';

$labels['folder_select'] = 'Sélectionner le dossier';
$labels['folder_unselect'] = 'Déselectionner le dossier';

$labels['reset_selected'] = 'Réinitialiser la sélection';
$labels['reset_transient'] = 'Réinitialiser les actifs';

$labels['status_title'] = 'Vue Dossier ...';

$labels['folder_expand_all'] = 'Développer tout';
$labels['folder_collapse_all'] = 'Réduire tout';

$labels['folder'] = 'Dossier';

$labels['reset'] = 'Réinitialiser';
$labels['apply'] = 'Appliquer';
$labels['cancel'] = 'Annuler';
$labels['select'] = 'Selectionner';
$labels['search'] = 'Rechercher';

$labels['source'] = 'Source';
$labels['target'] = 'Cible';

$labels['create'] = 'Créer';
$labels['delete'] = 'Supprimer';
$labels['rename'] = 'Renommer';
$labels['locate'] = 'Localiser';

$labels['parent'] = 'Parent';
$labels['header'] = 'En-tête';
$labels['format'] = 'Format';
$labels['type'] = 'Type';
$labels['full_name'] = 'Nom complet';
$labels['mail_addr'] = 'Adresse Mail';

$labels['message_copy'] = 'Copier vers...';
$labels['message_move'] = 'Déplacer vers...';

// settings

$labels['plugin_folder_menu'] = 'Plugin: Menu contextuel des dossiers';

$labels['activate_plugin'] = 'Activer le Plugin';

$labels['enable_logging'] = 'Activer les logs';
$labels['enable_refresh'] = 'Activer le rafraichissement des dossiers';
$labels['enable_folder_list_context_menu'] = 'Activer le menu contextuel pour les dossiers';
$labels['enable_folder_list_control_menu'] = 'Activer le menu de contrôle des dossiers';
$labels['enable_message_list_context_menu'] = 'Activer le menu contextuel pour les messages';

$labels['feature_choice'] = 'Activer les fonctionnalités du plugin';

$labels['filter_active'] = 'Filtres de la catégorie : "Actifs"';
$labels['filter_favorite'] = 'Filtres de la catégorie : "Favoris"';

$labels['predefined_list'] = 'Dossiers dans la catégorie "Favoris"';
$labels['contact_folder_format_list'] = 'Templates pour les contacts';

$labels['transient_expire_mins'] = 'Durée d\'expiration des dossiers "Actifs" (min)';
?>
