<?php
$options = array (
	'expires' => time() + 60*60*24*30,
	'path' => '/',
	'domain' => '',
	'secure' => true,
	'httponly' => false,
	'samesite' => 'None'
	);
setcookie('colorMode', ($_GET['darkmode']=='true' ? 'dark' : 'light'), $options);

http_response_code(200);
echo json_encode(array("theme" => ($_GET['darkmode']=='true' ? 'dark' : 'light')));
?>