Ce dépôt contient des scripts bash de notre conception permettant d'installer et configurer très rapidement votre propre serveur cloud "ALL SPARK". Ce serveur sécurisé sous Linux DEBIAN constitue la base de toutes les applications développées par notre association CYBERTRON. Il permet notamment de stocker et d'accéder à l'ensemble de vos données (fichiers, courriels, agendas, sauvegardes, bases de données) dans des formats ouverts. Le serveur ALL SPARK intègre également l'API de communication qui lui permet d'échanger avec d'autres applications (dont OPTIMUS AVOCATS).

Nos serveurs ALLSPARK n'intègrent que des logiciels libres et opensource garantissant une gratuité totale, une sécurité maximale et une transparence absolue.

Les scripts ont été conçus pour fonctionner sur une installation minimale Debian 10.

Pour plus d'informations, veuillez consulter notre [WIKI](https://wiki.cybertron.fr)
