#!/bin/bash
source /etc/allspark/functions.sh
if [ -z $MODULE_API_OPTIMUS ]; then require MODULE_API_OPTIMUS yesno "Voulez-vous installer le client OPTIMUS ?"; source /root/.allspark; fi
if [ -z $OPTIMUS_API_MARIADB_USER ]; then require OPTIMUS_API_MARIADB_USER string "Veuillez renseigner le nom de l'utilisateur MARIADB pour l'api OPTIMUS :"; source /root/.allspark; fi
if [ -z $OPTIMUS_API_MARIADB_PASSWORD ] || [ $OPTIMUS_API_MARIADB_PASSWORD = "auto" ]; then require OPTIMUS_API_MARIADB_PASSWORD password "Veuillez renseigner le mot de passe de l'utilisateur MARIADB pour l'api OPTIMUS :"; source /root/.allspark; fi
source /root/.allspark

if [ $MODULE_API_OPTIMUS = "Y" ]
then

  echo
  echo_green "==== INSTALLATION DU SERVICE OPTIMUS ===="

  echo_magenta "Installation de libreoffice (pour conversions de fichiers doc, odt, xls, pdf) ..."
  verbose apt-get -qq install libreoffice

  echo_magenta "Installation de pdftk (manipulation de documents pdf) ..."
  verbose apt-get -qq install pdftk

  echo_magenta "Installation de ooopy (manipulation de documents odt) ..."
  verbose apt-get -qq install python
  verbose wget -q https://files.pythonhosted.org/packages/13/d9/11061068960a5075b345f627416af56156a65213eab3c510e7a364a2329f/OOoPy-2.0.tar.gz -P /tmp
  verbose tar -xf /tmp/OOoPy-2.0.tar.gz -C /tmp
  cd /tmp/OOoPy-2.0
  verbose python setup.py install
  verbose rm -R /tmp/OOoPy-2.0
  cd /

  echo_magenta "Creation de l'utilisateur MARIADB"
  verbose mariadb -u root -e "GRANT SELECT ON server.users TO '$OPTIMUS_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$OPTIMUS_API_MARIADB_PASSWORD';"
  verbose mariadb -u root -e "FLUSH PRIVILEGES;"

  echo_magenta "Création d'une tâche automatique qui s'exécute chaque minute"
  cp /etc/allspark/optimus/optimus-api.timer /etc/systemd/system/optimus-api.timer
  cp /etc/allspark/optimus/optimus-api.service /etc/systemd/system/optimus-api.service
  systemctl -q enable optimus-api.timer
  systemctl -q start optimus-api.timer
  systemctl -q daemon-reload

  echo_magenta "installation du module api optimus-avocats"
  verbose rm -f -R /srv/api/api_optimus-avocats
  verbose mkdir /srv/api/api_optimus-avocats
  sudo git clone --quiet https://git.cybertron.fr/optimus/api-avocats /srv/api/api_optimus-avocats/.
  sed -i "s/OPTIMUS_API_MARIADB_USER/$OPTIMUS_API_MARIADB_USER/g" /srv/api/api_optimus-avocats/config.php
  sed -i "s/OPTIMUS_API_MARIADB_PASSWORD/$OPTIMUS_API_MARIADB_PASSWORD/g" /srv/api/api_optimus-avocats/config.php
  chown -R www-data:www-data /srv/api/api_optimus-avocats

  echo_magenta "installation du module api optimus-structures"
  verbose rm -f -R /srv/api/api_optimus-structures
  verbose mkdir /srv/api/api_optimus-structures
  sudo git clone --quiet https://git.cybertron.fr/optimus/api-structures /srv/api/api_optimus-structures/.
  sed -i "s/OPTIMUS_API_MARIADB_USER/$OPTIMUS_API_MARIADB_USER/g" /srv/api/api_optimus-structures/config.php
  sed -i "s/OPTIMUS_API_MARIADB_PASSWORD/$OPTIMUS_API_MARIADB_PASSWORD/g" /srv/api/api_optimus-structures/config.php
  chown -R www-data:www-data /srv/api/api_optimus-structures

  verbose mariadb -u root -e "REPLACE INTO server.services SET name='optimus-avocats', displayname='OPTIMUS Avocats', status=1, install='/etc/allspark/optimus-avocats/install.sh', uninstall=null, description='Outils de gestion pour avocats'"
  verbose mariadb -u root -e "REPLACE INTO server.services SET name='optimus-structures', displayname='OPTIMUS Structures', status=1, install='/etc/allspark/optimus-structures/install.sh', uninstall=null, description='Outils de gestion pour structure d\'exercice d\'avocats'"

fi
